import { FaLinkedinIn, FaGithub, FaFacebook } from "react-icons/fa";
import { MdAttachEmail } from "react-icons/md";
function Main() {
  return (
    <div className="w-full pt-28 text-center pb-24">
      <div className="max-w-[1240px] w-full h-full mx-auto  flex justify-center items-center">
        <div>
          <p className="uppercase text-gray-500 my-5 tracking-widest  ">
            Lets Build together
          </p>
          <h1>
            Hi I am <span className="text-rose-500">Client </span>
          </h1>
          <h1 className="py-2 text-gray-700">A front-End web developer</h1>
          <p className=" text-green-900-500">
            Image result for front end developer A front-end developer creates
            websites and applications using web languages such as HTML, CSS, and
            JavaScript that allow users to access and interact with the site or
            app. When you visit a website, the design elements you see were
            created by a front-end developer
          </p>
          <div className="py-3 ">
            <div className="flex items-center justify-between text-3xl max-w-[330px] m-auto w-full sm:w-[80%]">
              <div className="rounded-full  shadow-lg shadow-gray-400 p-3 cursor-pointer hover:scale-110 ease-in duration-300">
                <a
                  href="https://www.linkedin.com/in/tanjil-hossain-875b94216/"
                  target="blank"
                >
                  <FaLinkedinIn />
                </a>
              </div>
              <div className="rounded-full  shadow-lg shadow-gray-400 p-3 cursor-pointer hover:scale-110 ease-in duration-300">
                <a href="https://gitlab.com/tanjilhossain719" target="blank">
                  <FaGithub />
                </a>
              </div>
              <div className="rounded-full shadow-lg shadow-gray-400 p-3 cursor-pointer hover:scale-110 ease-in duration-300">
                <a
                  href="https://mail.google.com/mail/u/0/?fs=1&tf=cm&source=mailto&to=tanjilhossain719@gmail.com"
                  target="blank"
                >
                  <MdAttachEmail />
                </a>
              </div>
              <div className="rounded-full  shadow-lg shadow-gray-400 p-3 cursor-pointer hover:scale-110 ease-in duration-300">
                <a
                  href="https://www.facebook.com/treasure.turjo"
                  target="blank"
                >
                  <FaFacebook />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Main;
